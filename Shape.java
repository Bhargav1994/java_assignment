/* Assignment # 2

Create a class hierarchy :
  							Shape
                   Rectangle Circle Triangle
Override method calculateArea() from Shape class in its each subclass
Create an array of Shape references & observe dynamic polymorphism */


package Shape_class;

class Rectangle extends Shape
{
	@Override
	public void area_calculations(double a, double b)
	{
		System.out.println("Area of Rectangle is ==" + (a * b));
	}
}

class Traingle extends Shape
{
	@Override
	public void area_calculations(double a, double b)
	{
		System.out.println("Area of Traingle is ==" + ((a * b)/2));
	}
}

class Circle extends Shape
{
	@Override
	public void area_calculations(double a, double b)
	{
		System.out.println("Area of Circle is == " + ((a * a * 22)/7));
	}
}
public abstract class Shape {
	
	public abstract void area_calculations(double a, double b);


	public static void main(String [] args)
	{	
		Rectangle rect1 = new Rectangle();
		Circle circ1 = new Circle();
		Traingle trian1 = new Traingle();
		Shape array[]= {rect1,circ1,trian1};
		for (int i=0;i<3;i++)
		{
			array[i].area_calculations(4, 4); //Shape array as references
				
		}

	}
	
	
}

